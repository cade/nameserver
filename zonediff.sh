#!/bin/bash

DIFF=$(command -v colordiff)
if [ -z "${DIFF}" ]; then
	DIFF=$(command -v diff)
	if [ -z "${DIFF}" ]; then
		echo Can not find diff or colordiff, abort...
		exit 1
	fi
fi

if [ $(pwd) != /etc/nsd/GIT ]; then
	echo Please start in /etc/nsd/GIT, abort...
	exit 1
fi

for i in *.zone nsd.conf devuan-slave.conf; do
	if [ -f ../${i} ]; then
		${DIFF} -u ../${i} ${i}
	fi
done

