#!/bin/bash

if [ "$#" -lt "1" ]; then
	echo Usage: $0 'domain1.name [domain2.name ...]'
	exit
fi

for d in "$@"; do
	for n in localhost ns{2..6}.devuan.dev dns.google; do
		echo ' *** '$n' check '$d
		nslookup $d $n|grep ^Address:|tail -n +2|sed -e 's/Address: /\t\t/'
	done
	echo -{1..50}|sed 's/[0-9]* \?//g'
done
