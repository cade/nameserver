;
; This is NSD's master zone file for devuan.org
; (it has bind9 format)

$ORIGIN devuan.org.
$TTL 1800

@ IN SOA ns2.devuan.dev. hostmaster.devuan.org. (
	2024051901 ; serial number for this zone file
	3600 ; refresh was 21600
	1800 ; retry was 7200
	691200 ; expire
	1800 ) ; default TTL and end of header

; NS
@					IN NS ns2.devuan.dev.
@					IN NS ns3.devuan.dev.
@					IN NS ns4.devuan.dev.
@					IN NS ns5.devuan.dev.
@					IN NS ns6.devuan.dev.

; MX
@					IN MX 10 email.devuan.org.
; this allows mx.devuan.org + all public ips on the infra... may need to get more specific
; order is
;    bonito 85.10.193.185 116.202.138.208/28 2a01:4f8:a0:61f5::1  2a01:4f8:fff1:d::/64
;    polpo  65.21.79.241  95.217.249.224/28  2a01:4f9:3b:5459::1  2a01:4f9:fff1:13::/64
;    seppia 5.9.97.46     136.243.229.208/28 2a01:4f8:162:2423::1 2a01:4f8:fff3:1f::/64
@					IN TXT ( "v=spf1"
						; bonito
						" ip4:85.10.193.185 ip4:116.202.138.208/28 ip6:2a01:4f8:a0:61f5::1 ip6:2a01:4f8:fff1:d::/64"
						; polpo
						" ip4:65.21.79.241 ip4:95.217.249.224/28 ip6:2a01:4f9:3b:5459::1 ip6:2a01:4f9:fff1:13::/64"
						; seppia
						" ip4:5.9.97.46 ip4:136.243.229.208/28 ip6:2a01:4f8:162:2423::1 ip6:2a01:4f8:fff3:1f::/64"
						; dyne
						" include:dyne.org"
						" -all" )

default._domainkey IN TXT ( "v=DKIM1; h=sha256; k=rsa; "
	"p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAw93vHx1CGMchK29DiTC1qJBtiLxMg4AAi1odaqnzzd6zTVElV1HoRjxUYdokf0gy3wvj/Gy4lekwPgn43H81aF9XA9Xw4D/IZRr1XxxuS4Wkj0UkNP1skqvdxO7MXG0DTOVgljgOcTb7PUfpbKLNsOKRdb1GBhYIpvtLIyhD5ucrqJiwx9oE4uWWP/aTXCpDqAgCGc26AAWrSr"
	"lNnw00sF5xU56lyVwYAPjcdJOQVroLsu8QQVIXsrF797Yvo7ZvLkWbK4ZKKhN3oVW6wdDWtC5+i05sqWh2crgP0kyRK1NJR7DM0MhXkVIEmoArok4+IQ3kTWlZQ8RHGz/6mMHhVwIDAQAB" )  ; ----- DKIM key default for devuan.org
_dmarc IN TXT ( "v=DMARC1;p=none;sp=none;pct=100;rua=mailto:dmarc@devuan.org;ruf=mailto:dmarc@devuan.org;ri=86400;aspf=s;adkim=r;fo=1" )

; google search console
@ 10800 IN TXT "google-site-verification=fuFkyssi2roSMqU1CC3JtkActMxUsbScjmaFM_GsEEM"

; A / AAAA

; mail server(s)
mx					IN CNAME email.devuan.org.
mail				IN CNAME email.devuan.org.
email				IN A 95.217.249.228 ; via seppia
email				IN AAAA 2a01:4f9:fff1:13::5fd9:f9e4 ; via seppia

; TLSA records
_443._tcp.beta		IN TLSA 2 0 1 7fdce3bf4103c2684b3adbb5792884bd45c75094c217788863950346f79c90a3
_443._tcp.ci		IN TLSA 2 0 1 7fdce3bf4103c2684b3adbb5792884bd45c75094c217788863950346f79c90a3
_443._tcp.git		IN TLSA 2 0 1 7fdce3bf4103c2684b3adbb5792884bd45c75094c217788863950346f79c90a3
_443._tcp.www		IN TLSA 3 0 1 b91b36d5929a8617ee57781c35620b1fed8bdc653f9a29ea7317736530a15ebf

; "devuan.org"
@					IN A 116.202.138.214 ; via bonito
www					IN A 116.202.138.214 ; via bonito
@					IN AAAA 2a01:4f8:fff1:d::74ca:8ad6 ; via bonito
www					IN AAAA 2a01:4f8:fff1:d::74ca:8ad6 ; via bonito

; default for country codes without mirrors
; setting that to be a CNAME of deb.devuan.org is not possible without reconfiguring all mirrors in the round robin
; xx.deb.devuan.org redirects all requests to deb.devuan.org
*.deb				IN CNAME xx.deb.devuan.org.

api.pkginfo			IN CNAME pkgmaster.devuan.org.
archive				IN CNAME pkgmaster.devuan.org.
arm-files			IN CNAME files.devuan.org.
beta				IN CNAME www.devuan.org.
bugs				IN A 116.202.138.208 ; via bonito
bugs				IN AAAA 2a01:4f8:fff1:d::74ca:8ad0 ; via bonito
bugs				IN MX 10 email.devuan.org.
bugs				IN TXT ( "v=spf1 include:devuan.org" )
ci					IN A 116.202.138.209 ; via bonito
ci					IN AAAA 2a01:4f8:fff1:d::74ca:8ad1 ; via bonito
files				IN A 136.243.229.209 ; via seppia
files				IN AAAA 2a01:4f8:fff3:1f::88f3:e5d1 ; via seppia
forum				IN CNAME dev1galaxy.org.
galaxy				IN CNAME dev1galaxy.org.
git					IN CNAME gitea.devuan.org.
keyring				IN CNAME gitea.devuan.org.
gitea				IN A 116.202.138.212 ; gitea.devuan.dev. ; via bonito
gitea				IN AAAA 2a01:4f8:fff1:d::74ca:8ad4 ; via bonito
gitea				IN TXT ( "v=spf1 include:devuan.org" )
bonito				IN A 85.10.193.185
bonito				IN AAAA 2a01:4f8:a0:61f5::1
polpo				IN A 65.21.79.241
polpo				IN AAAA 2a01:4f9:3b:5459::1
seppia				IN A 5.9.97.46
seppia				IN AAAA 2a01:4f8:162:2423::1
packages			IN CNAME pkgmaster.devuan.org.
pkginfo				IN CNAME www.devuan.org.
pkgmaster			IN A 116.202.138.216 ; via bonito
pkgmaster			IN AAAA 2a01:4f8:fff1:d::74ca:8ad8 ; via bonito
popcon				IN A 116.202.138.208 ; bugs.devuan.org. ; via bonito
popcon				IN AAAA 2a01:4f8:fff1:d::74ca:8ad0 ; via bonito
popcon				IN MX 10 email.devuan.org.
veritas				IN A 95.217.249.226 ; via polpo
veritas				IN AAAA 2a01:4f9:fff1:13::5fd9:f9e2 ; via polpo
webmail				IN A 95.217.249.225 ; via polpo
webmail				IN AAAA 2a01:4f9:fff1:13::5fd9:f9e1 ; via polpo

; Other hosts

deb					IN CNAME deb.rr.devuan.org.
; dns round robin on Devuan infra
rr					IN NS ns2.devuan.dev.
rr					IN NS ns3.devuan.dev.
rr					IN NS ns4.devuan.dev.
rr					IN NS ns5.devuan.dev.
rr					IN NS ns6.devuan.dev.
wiki				IN A 136.243.229.211 ; via seppia
wiki				IN AAAA 2a01:4f8:fff3:1f::88f3:e5d3 ; via seppia

; external hosts

; apt-panopticon instance in Mathura, India
ap.in				IN A 103.146.168.109
ap.in				IN AAAA 2407:b6c0::109
; backup instance in Mathura, India (rsyncs from Sofia)
bu.in				IN A 103.146.168.13
bu.in				IN AAAA 2407:b6c0::13
; backup instance in Sofia, Bulgaria (pushed from backup)
bu.bg				IN A 195.85.215.139
bu.bg				IN AAAA 2a01:9e40::139

; country code delegation

; fallback entry for country codes without mirrors behind
; this is doing a redirect to deb.devuan.org; note that pointing the domain name to
; the respective mirrors may not work because they may not be configured to handle
; that country code
xx.deb				IN A 116.202.138.217 ; via bonito on pkgmirror
xx.deb				IN AAAA 2a01:4f8:fff1:d::74ca:8ad9 ; via bonito on pkgmirror

ad.deb				IN CNAME ad.deb.rr ; Andorra
ae.deb				IN CNAME ae.deb.rr ; United Arab Emirates
af.deb				IN CNAME af.deb.rr ; Afghanistan
ag.deb				IN CNAME ag.deb.rr ; Antigua and Barbuda
ai.deb				IN CNAME ai.deb.rr ; Anguilla
al.deb				IN CNAME al.deb.rr ; Albania
am.deb				IN CNAME am.deb.rr ; Armenia
ao.deb				IN CNAME ao.deb.rr ; Angola
aq.deb				IN CNAME aq.deb.rr ; Antarctica
ar.deb				IN CNAME ar.deb.rr ; Argentina
as.deb				IN CNAME as.deb.rr ; American Samoa
at.deb				IN CNAME at.deb.rr ; Austria
au.deb				IN CNAME au.deb.rr ; Australia
aw.deb				IN CNAME aw.deb.rr ; Aruba
ax.deb				IN CNAME ax.deb.rr ; Aland Islands
az.deb				IN CNAME az.deb.rr ; Azerbaijan
ba.deb				IN CNAME ba.deb.rr ; Bosnia and Herzegovina
bb.deb				IN CNAME bb.deb.rr ; Barbados
bd.deb				IN CNAME bd.deb.rr ; Bangladesh
be.deb				IN CNAME be.deb.rr ; Belgium
bf.deb				IN CNAME bf.deb.rr ; Burkina Faso
bg.deb				IN CNAME bg.deb.rr ; Bulgaria
bh.deb				IN CNAME bh.deb.rr ; Bahrain
bi.deb				IN CNAME bi.deb.rr ; Burundi
bj.deb				IN CNAME bj.deb.rr ; Benin
bl.deb				IN CNAME bl.deb.rr ; Saint Barthelemey
bm.deb				IN CNAME bm.deb.rr ; Bermuda
bn.deb				IN CNAME bn.deb.rr ; Brunei Darussalam
bo.deb				IN CNAME bo.deb.rr ; Bolivia
bq.deb				IN CNAME bq.deb.rr ; Bonaire, Saint Eustatius and Saba
br.deb				IN CNAME br.deb.rr ; Brazil
bs.deb				IN CNAME bs.deb.rr ; Bahamas
bt.deb				IN CNAME bt.deb.rr ; Bhutan
bv.deb				IN CNAME bv.deb.rr ; Bouvet Island
bw.deb				IN CNAME bw.deb.rr ; Botswana
by.deb				IN CNAME by.deb.rr ; Belarus
bz.deb				IN CNAME bz.deb.rr ; Belize
ca.deb				IN CNAME ca.deb.rr ; Canada
cc.deb				IN CNAME cc.deb.rr ; Cocos (Keeling) Islands
cd.deb				IN CNAME cd.deb.rr ; Congo, The Democratic Republic of the
cf.deb				IN CNAME cf.deb.rr ; Central African Republic
cg.deb				IN CNAME cg.deb.rr ; Congo
ch.deb				IN CNAME ch.deb.rr ; Switzerland
ci.deb				IN CNAME ci.deb.rr ; Cote d'Ivoire
ck.deb				IN CNAME ck.deb.rr ; Cook Islands
cl.deb				IN CNAME cl.deb.rr ; Chile
cm.deb				IN CNAME cm.deb.rr ; Cameroon
cn.deb				IN CNAME cn.deb.rr ; China
co.deb				IN CNAME co.deb.rr ; Colombia
cr.deb				IN CNAME cr.deb.rr ; Costa Rica
cu.deb				IN CNAME cu.deb.rr ; Cuba
cv.deb				IN CNAME cv.deb.rr ; Cape Verde
cw.deb				IN CNAME cw.deb.rr ; Curacao
cx.deb				IN CNAME cx.deb.rr ; Christmas Island
cy.deb				IN CNAME cy.deb.rr ; Cyprus
cz.deb				IN CNAME cz.deb.rr ; Czech Republic
de.deb				IN CNAME de.deb.rr ; Germany
dj.deb				IN CNAME dj.deb.rr ; Djibouti
dk.deb				IN CNAME dk.deb.rr ; Denmark
dm.deb				IN CNAME dm.deb.rr ; Dominica
do.deb				IN CNAME do.deb.rr ; Dominican Republic
dz.deb				IN CNAME dz.deb.rr ; Algeria
ec.deb				IN CNAME ec.deb.rr ; Ecuador
ee.deb				IN CNAME ee.deb.rr ; Estonia
eg.deb				IN CNAME eg.deb.rr ; Egypt
er.deb				IN CNAME er.deb.rr ; Eritrea
es.deb				IN CNAME es.deb.rr ; Spain
et.deb				IN CNAME et.deb.rr ; Ethiopia
fi.deb				IN CNAME fi.deb.rr ; Finland
fj.deb				IN CNAME fj.deb.rr ; Fiji
fk.deb				IN CNAME fk.deb.rr ; Falkland Islands (Malvinas)
fm.deb				IN CNAME fm.deb.rr ; Micronesia, Federated States of
fo.deb				IN CNAME fo.deb.rr ; Faroe Islands
fr.deb				IN CNAME fr.deb.rr ; France
ga.deb				IN CNAME ga.deb.rr ; Gabon
gb.deb				IN CNAME gb.deb.rr ; United Kingdom
gd.deb				IN CNAME gd.deb.rr ; Grenada
ge.deb				IN CNAME ge.deb.rr ; Georgia
gf.deb				IN CNAME gf.deb.rr ; French Guiana
gg.deb				IN CNAME gg.deb.rr ; Guernsey
gh.deb				IN CNAME gh.deb.rr ; Ghana
gi.deb				IN CNAME gi.deb.rr ; Gibraltar
gl.deb				IN CNAME gl.deb.rr ; Greenland
gm.deb				IN CNAME gm.deb.rr ; Gambia
gn.deb				IN CNAME gn.deb.rr ; Guinea
gp.deb				IN CNAME gp.deb.rr ; Guadeloupe
gq.deb				IN CNAME gq.deb.rr ; Equatorial Guinea
gr.deb				IN CNAME gr.deb.rr ; Greece
gs.deb				IN CNAME gs.deb.rr ; South Georgia and the South Sandwich Islands
gt.deb				IN CNAME gt.deb.rr ; Guatemala
gu.deb				IN CNAME gu.deb.rr ; Guam
gw.deb				IN CNAME gw.deb.rr ; Guinea-Bissau
gy.deb				IN CNAME gy.deb.rr ; Guyana
hk.deb				IN CNAME hk.deb.rr ; Hong Kong
hm.deb				IN CNAME hm.deb.rr ; Heard Island and McDonald Islands
hn.deb				IN CNAME hn.deb.rr ; Honduras
hr.deb				IN CNAME hr.deb.rr ; Croatia
ht.deb				IN CNAME ht.deb.rr ; Haiti
hu.deb				IN CNAME hu.deb.rr ; Hungary
id.deb				IN CNAME id.deb.rr ; Indonesia
ie.deb				IN CNAME ie.deb.rr ; Ireland
il.deb				IN CNAME il.deb.rr ; Israel
im.deb				IN CNAME im.deb.rr ; Isle of Man
in.deb				IN CNAME in.deb.rr ; India
io.deb				IN CNAME io.deb.rr ; British Indian Ocean Territory
iq.deb				IN CNAME iq.deb.rr ; Iraq
ir.deb				IN CNAME ir.deb.rr ; Iran, Islamic Republic of
is.deb				IN CNAME is.deb.rr ; Iceland
it.deb				IN CNAME it.deb.rr ; Italy
je.deb				IN CNAME je.deb.rr ; Jersey
jm.deb				IN CNAME jm.deb.rr ; Jamaica
jo.deb				IN CNAME jo.deb.rr ; Jordan
jp.deb				IN CNAME jp.deb.rr ; Japan
ke.deb				IN CNAME ke.deb.rr ; Kenya
kg.deb				IN CNAME kg.deb.rr ; Kyrgyzstan
kh.deb				IN CNAME kh.deb.rr ; Cambodia
ki.deb				IN CNAME ki.deb.rr ; Kiribati
km.deb				IN CNAME km.deb.rr ; Comoros
kn.deb				IN CNAME kn.deb.rr ; Saint Kitts and Nevis
kp.deb				IN CNAME kp.deb.rr ; Korea, Democratic People's Republic of
kr.deb				IN CNAME kr.deb.rr ; Korea, Republic of
kw.deb				IN CNAME kw.deb.rr ; Kuwait
ky.deb				IN CNAME ky.deb.rr ; Cayman Islands
kz.deb				IN CNAME kz.deb.rr ; Kazakhstan
la.deb				IN CNAME la.deb.rr ; Lao People's Democratic Republic
lb.deb				IN CNAME lb.deb.rr ; Lebanon
lc.deb				IN CNAME lc.deb.rr ; Saint Lucia
li.deb				IN CNAME li.deb.rr ; Liechtenstein
lk.deb				IN CNAME lk.deb.rr ; Sri Lanka
lr.deb				IN CNAME lr.deb.rr ; Liberia
ls.deb				IN CNAME ls.deb.rr ; Lesotho
lt.deb				IN CNAME lt.deb.rr ; Lithuania
lu.deb				IN CNAME lu.deb.rr ; Luxembourg
lv.deb				IN CNAME lv.deb.rr ; Latvia
ly.deb				IN CNAME ly.deb.rr ; Libyan Arab Jamahiriya
ma.deb				IN CNAME ma.deb.rr ; Morocco
mc.deb				IN CNAME mc.deb.rr ; Monaco
md.deb				IN CNAME md.deb.rr ; Moldova, Republic of
me.deb				IN CNAME me.deb.rr ; Montenegro
mf.deb				IN CNAME mf.deb.rr ; Saint Martin
mg.deb				IN CNAME mg.deb.rr ; Madagascar
mh.deb				IN CNAME mh.deb.rr ; Marshall Islands
mk.deb				IN CNAME mk.deb.rr ; Macedonia
ml.deb				IN CNAME ml.deb.rr ; Mali
mm.deb				IN CNAME mm.deb.rr ; Myanmar
mn.deb				IN CNAME mn.deb.rr ; Mongolia
mo.deb				IN CNAME mo.deb.rr ; Macao
mp.deb				IN CNAME mp.deb.rr ; Northern Mariana Islands
mq.deb				IN CNAME mq.deb.rr ; Martinique
mr.deb				IN CNAME mr.deb.rr ; Mauritania
ms.deb				IN CNAME ms.deb.rr ; Montserrat
mt.deb				IN CNAME mt.deb.rr ; Malta
mu.deb				IN CNAME mu.deb.rr ; Mauritius
mv.deb				IN CNAME mv.deb.rr ; Maldives
mw.deb				IN CNAME mw.deb.rr ; Malawi
mx.deb				IN CNAME mx.deb.rr ; Mexico
my.deb				IN CNAME my.deb.rr ; Malaysia
mz.deb				IN CNAME mz.deb.rr ; Mozambique
na.deb				IN CNAME na.deb.rr ; Namibia
nc.deb				IN CNAME nc.deb.rr ; New Caledonia
ne.deb				IN CNAME ne.deb.rr ; Niger
nf.deb				IN CNAME nf.deb.rr ; Norfolk Island
ng.deb				IN CNAME ng.deb.rr ; Nigeria
ni.deb				IN CNAME ni.deb.rr ; Nicaragua
nl.deb				IN CNAME nl.deb.rr ; Netherlands
no.deb				IN CNAME no.deb.rr ; Norway
np.deb				IN CNAME np.deb.rr ; Nepal
nr.deb				IN CNAME nr.deb.rr ; Nauru
nu.deb				IN CNAME nu.deb.rr ; Niue
nz.deb				IN CNAME nz.deb.rr ; New Zealand
om.deb				IN CNAME om.deb.rr ; Oman
pa.deb				IN CNAME pa.deb.rr ; Panama
pe.deb				IN CNAME pe.deb.rr ; Peru
pf.deb				IN CNAME pf.deb.rr ; French Polynesia
pg.deb				IN CNAME pg.deb.rr ; Papua New Guinea
ph.deb				IN CNAME ph.deb.rr ; Philippines
pk.deb				IN CNAME pk.deb.rr ; Pakistan
pl.deb				IN CNAME pl.deb.rr ; Poland
pm.deb				IN CNAME pm.deb.rr ; Saint Pierre and Miquelon
pn.deb				IN CNAME pn.deb.rr ; Pitcairn
pr.deb				IN CNAME pr.deb.rr ; Puerto Rico
ps.deb				IN CNAME ps.deb.rr ; Palestinian Territory
pt.deb				IN CNAME pt.deb.rr ; Portugal
pw.deb				IN CNAME pw.deb.rr ; Palau
py.deb				IN CNAME py.deb.rr ; Paraguay
qa.deb				IN CNAME qa.deb.rr ; Qatar
re.deb				IN CNAME re.deb.rr ; Reunion
ro.deb				IN CNAME ro.deb.rr ; Romania
rs.deb				IN CNAME rs.deb.rr ; Serbia
ru.deb				IN CNAME ru.deb.rr ; Russian Federation
rw.deb				IN CNAME rw.deb.rr ; Rwanda
sa.deb				IN CNAME sa.deb.rr ; Saudi Arabia
sb.deb				IN CNAME sb.deb.rr ; Solomon Islands
sc.deb				IN CNAME sc.deb.rr ; Seychelles
sd.deb				IN CNAME sd.deb.rr ; Sudan
se.deb				IN CNAME se.deb.rr ; Sweden
sg.deb				IN CNAME sg.deb.rr ; Singapore
sh.deb				IN CNAME sh.deb.rr ; Saint Helena
si.deb				IN CNAME si.deb.rr ; Slovenia
sj.deb				IN CNAME sj.deb.rr ; Svalbard and Jan Mayen
sk.deb				IN CNAME sk.deb.rr ; Slovakia
sl.deb				IN CNAME sl.deb.rr ; Sierra Leone
sm.deb				IN CNAME sm.deb.rr ; San Marino
sn.deb				IN CNAME sn.deb.rr ; Senegal
so.deb				IN CNAME so.deb.rr ; Somalia
sr.deb				IN CNAME sr.deb.rr ; Suriname
ss.deb				IN CNAME ss.deb.rr ; South Sudan
st.deb				IN CNAME st.deb.rr ; Sao Tome and Principe
sv.deb				IN CNAME sv.deb.rr ; El Salvador
sx.deb				IN CNAME sx.deb.rr ; Sint Maarten
sy.deb				IN CNAME sy.deb.rr ; Syrian Arab Republic
sz.deb				IN CNAME sz.deb.rr ; Swaziland
tc.deb				IN CNAME tc.deb.rr ; Turks and Caicos Islands
td.deb				IN CNAME td.deb.rr ; Chad
tf.deb				IN CNAME tf.deb.rr ; French Southern Territories
tg.deb				IN CNAME tg.deb.rr ; Togo
th.deb				IN CNAME th.deb.rr ; Thailand
tj.deb				IN CNAME tj.deb.rr ; Tajikistan
tk.deb				IN CNAME tk.deb.rr ; Tokelau
tl.deb				IN CNAME tl.deb.rr ; Timor-Leste
tm.deb				IN CNAME tm.deb.rr ; Turkmenistan
tn.deb				IN CNAME tn.deb.rr ; Tunisia
to.deb				IN CNAME to.deb.rr ; Tonga
tr.deb				IN CNAME tr.deb.rr ; Turkey
tt.deb				IN CNAME tt.deb.rr ; Trinidad and Tobago
tv.deb				IN CNAME tv.deb.rr ; Tuvalu
tw.deb				IN CNAME tw.deb.rr ; Taiwan
tz.deb				IN CNAME tz.deb.rr ; Tanzania, United Republic of
ua.deb				IN CNAME ua.deb.rr ; Ukraine
ug.deb				IN CNAME ug.deb.rr ; Uganda
um.deb				IN CNAME um.deb.rr ; United States Minor Outlying Islands
us.deb				IN CNAME us.deb.rr ; United States
uy.deb				IN CNAME uy.deb.rr ; Uruguay
uz.deb				IN CNAME uz.deb.rr ; Uzbekistan
va.deb				IN CNAME va.deb.rr ; Holy See (Vatican City State)
vc.deb				IN CNAME vc.deb.rr ; Saint Vincent and the Grenadines
ve.deb				IN CNAME ve.deb.rr ; Venezuela
vg.deb				IN CNAME vg.deb.rr ; Virgin Islands, British
vi.deb				IN CNAME vi.deb.rr ; Virgin Islands, U.S.
vn.deb				IN CNAME vn.deb.rr ; Vietnam
vu.deb				IN CNAME vu.deb.rr ; Vanuatu
wf.deb				IN CNAME wf.deb.rr ; Wallis and Futuna
ws.deb				IN CNAME ws.deb.rr ; Samoa
ye.deb				IN CNAME ye.deb.rr ; Yemen
yt.deb				IN CNAME yt.deb.rr ; Mayotte
za.deb				IN CNAME za.deb.rr ; South Africa
zm.deb				IN CNAME zm.deb.rr ; Zambia
zw.deb				IN CNAME zw.deb.rr ; Zimbabwe
